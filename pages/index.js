import React, { Component } from 'react';
import TodoList from '../components/TodoList';
import TodoForm from '../components/TodoForm';

class index extends Component {
  render() {
    return (
      <div>
        <TodoList />
        <TodoForm />
      </div>
    );
  }
}

export default index;