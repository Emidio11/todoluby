import React from 'react';
import { Formik, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';

import { FormikForm } from './styles';
import { addTodo } from '../../redux/actions/todoActions';
import { v1 as uuid } from 'uuid';
import { useDispatch } from 'react-redux';


const validations = yup.object().shape({
    content: yup.string()
    .min(5, 'Must have 5 digits or more!')
    .required()
})

function Form() {
    const dispatch = useDispatch();

    return (
        <Formik 
            initialValues={{
                content: '',
            }}
            onSubmit={values => (
                dispatch(
                    addTodo({
                        id: uuid(),
                        content: values.content,
                    }
                ), 
                values.content = "")
            )}

            validationSchema={validations}
        >
            <FormikForm>
                <Field type="text" name="content" placeholder="Todo" />
                <ErrorMessage content="span" name="content" />
                <button type="submit">Add Todo</button>
            </FormikForm>
        </Formik>
    )
}

export default Form;