import styled from 'styled-components';

export const Container = styled.div`
    max-width: 1200px;
    width: 100%;
    margin: auto;
    display: flex;
    flex-direction: column;
    align-items: center;
`

export const Header = styled.h1`
    color: blue;
`

export const List = styled.div`
    widith: 100%;
`

export const Todo = styled.div`
    border: 1px solid gray;
    max-width: 100%;
    padding: 10px;
`