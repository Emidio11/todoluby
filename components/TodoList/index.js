import React from 'react';
import { useSelector } from 'react-redux';

import {Container, Header, List, Todo} from './styles';

const Todos = () => {
    let todos = useSelector(state => state.todo);

    const todoList = todos.length ? (
        todos.map(todo => {
            return (
                <Todo key={todo.id}>{todo.content}</Todo>
            )
        })
    ) : (
        <p>No todos found</p>
    )

    return (
        <Container>
            <Header>Todo List</Header>
            <List>{todoList}</List>
        </Container>
    );
}

export default Todos;