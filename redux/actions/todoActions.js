import { ADD_TODO, UPDATE_TODO, DELETE_TODO } from "../types";

export function addTodo(todo) {
    return {
        type: ADD_TODO,
        payload: todo,
    }
}

export function updateTodo(todo) {
    return {
        type: UPDATE_TODO,
        payload: todo,
    }
}

export function deleteTodo(todoId) {
    return {
        type: DELETE_TODO,
        payload: todoId,
    }
}